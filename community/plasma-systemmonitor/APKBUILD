# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-systemmonitor
pkgver=5.24.1
pkgrel=0
pkgdesc="An application for monitoring system resources"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	knewstuff-dev
	kservice-dev
	libksysguard-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-systemmonitor-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ksystemstatstest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
57a87cce781931a296913d7fea12796276435fa10705eca2938a12dfd46bcc6995bdd5a73f135188ae284d6f0ef52259dc4b3bc9aec4d6aaa349e69ddfdba0dd  plasma-systemmonitor-5.24.1.tar.xz
"
