# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=go-ipfs
pkgver=0.12.0
pkgrel=0
pkgdesc="Inter Platnetary File System (IPFS), a peer-to-peer hypermedia distribution protocol"
url="https://ipfs.io/"
# aarch64 segfaults on builders
arch="x86_64 x86 armhf armv7"
license="MIT Apache-2.0"
pkgusers="ipfs"
pkggroups="ipfs"
options="chmod-clean !check" # No test suite from upstream
makedepends="make go bash binutils-gold git"
install="$pkgname.pre-install $pkgname.post-install"
subpackages="$pkgname-doc $pkgname-openrc $pkgname-bash-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/ipfs/go-ipfs/archive/v$pkgver.tar.gz
	ipfs.initd
	ipfs.confd"
builddir="$srcdir/src/github.com/ipfs/go-ipfs"

# secfixes:
#   0.8.0-r0:
#     - CVE-2020-26279
#     - CVE-2020-26283

prepare() {
	export GOPATH="$srcdir"

	mkdir -p  "$(dirname $builddir)"
	mv "$srcdir"/$pkgname-$pkgver "$builddir"/

	default_prepare
}

build() {
	export GOPATH="$srcdir"
	export GOBIN="$GOPATH/bin"

	make build

	# build bash completion
	./cmd/ipfs/ipfs commands completion bash > $pkgname.bash
}

package() {
	install -m755 -D cmd/ipfs/ipfs \
		"$pkgdir"/usr/bin/ipfs

	install -m644 -D -t "$pkgdir/usr/share/doc/$pkgname" README.md

	install -m755 -D "$srcdir"/ipfs.initd \
		"$pkgdir"/etc/init.d/ipfs
	install -m644 -D "$srcdir"/ipfs.confd \
		"$pkgdir"/etc/conf.d/ipfs

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname

	install -dm750 -o ipfs -g ipfs "$pkgdir"/var/lib/ipfs
	install -dm755 "$pkgdir"/var/log/ipfs
}

sha512sums="
90ed46b3a399f9038232504f08504aa66f3053f311ca618de4de89e4318f6c0623a92b499ca674d21d13ebe42ee3c2f97377020d06ebca2063a472e5d3e4890f  go-ipfs-0.12.0.tar.gz
3e51e9a3dca1b991e8549f8354f7c2cfd1bb9b73d7a59557878d5c9ab4189988676d789172af3ba1fd57193ec48ca9125919507b0de7d0400ce0d6166622e556  ipfs.initd
c55afeb3efe381d18258ddf00f58325b77156375cf223fb2daa049df056efe22e9139cce0f81dc4c73759dad5097af5f3201414beb5950bd894df9ae8c7c4ed1  ipfs.confd
"
