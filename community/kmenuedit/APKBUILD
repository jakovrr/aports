# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmenuedit
pkgver=5.24.1
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt5-qtbase-dev
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4110e7f3b2b79f4667ce4b01a975486348815a42d65a8c30c1088a4f6b447a720a67d49be2153ecaf0296e1b999ffe367309f479c852997128c783b4ebc0f7b0  kmenuedit-5.24.1.tar.xz
"
