# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=xdg-desktop-portal-kde
pkgver=5.24.1
pkgrel=0
pkgdesc="A backend implementation for xdg-desktop-portal that is using Qt/KDE"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !s390x !riscv64"
url="https://phabricator.kde.org/source/xdg-desktop-portal-kde"
license="LGPL-2.0-or-later"
depends="xdg-desktop-portal"
makedepends="
	cups-dev
	extra-cmake-modules
	glib-dev
	kcoreaddons-dev
	kdeclarative-dev
	kio-dev
	kirigami2-dev
	kwayland-dev
	libepoxy-dev
	pipewire-dev
	plasma-framework-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	"
subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/xdg-desktop-portal-kde-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7b776adec107b65b78595f5884b514a752176e71f99cf4f50aae0483d837cdd2d5082ed08a89cf85c88cddc761cd2319b875655ad4bfe2cc963a67b93625bac1  xdg-desktop-portal-kde-5.24.1.tar.xz
"
