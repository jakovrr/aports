# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kparts
pkgver=5.91.0
pkgrel=0
pkgdesc="Document centric plugin system"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kparts-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
0f40402c72dbf8a3e5d7e8d66aa380c35b1812dda8e573b0501c180a034783903f8c240b6e1b8e02b449785ab20de9bf3a8bce4bec44673356abc7872c053bde  kparts-5.91.0.tar.xz
"
