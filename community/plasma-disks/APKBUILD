# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-disks
pkgver=5.24.1
pkgrel=0
pkgdesc="Monitors S.M.A.R.T. capable devices for imminent failure"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	smartmontools
	"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kdbusaddons-dev
	knotifications-dev
	ki18n-dev
	solid-dev
	kservice-dev
	kio-dev
	kauth-dev
	kdeclarative-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-disks-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a1980cbc6098690beb392324d69533a1057bf8c938ad19ad3031a515a0a01827188d30bf80bfcbfde059557857f8ed6d92c687ea06687cd66f0ffcce4bcf2e25  plasma-disks-5.24.1.tar.xz
"
