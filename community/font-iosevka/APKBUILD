# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=14.0.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
a1bd7f4c52acfa9ef09428b527422e350d26f64063b5f2149e0db497988284cb1c29d0bfbe34182fcfcf20c8eae265c5d043721e506e559becf0c2e746c88d02  super-ttc-iosevka-14.0.1.zip
05b70b6f95d15a3aaf3e448f81e2eec0c5d6b877f101be453892e1d55d42170fe50468e2be000c2ff83fae6155e9a4b88549dee72291cfc175f9bf3f84f6b910  super-ttc-iosevka-slab-14.0.1.zip
56368298bd0c4aafdc0b6b8e3e8981048634a6bc2fcaa22bf6372bfaf10552d0681be507b60a96a59293bcaa3510988409e82031e3347f6cedd4804a6683352f  super-ttc-iosevka-curly-14.0.1.zip
2774d8d6915f6af3335da34134ec906ab0bfccba478e3702de46ca273c45ef7d032dd5cbb4eb3dbadb316b5f744e170be943595a989fdf49640e4f072baf7612  super-ttc-iosevka-curly-slab-14.0.1.zip
127fce5d59b26abcf77616de9491e76ac8011d020ea1828c67aff128f51471c919303f6cb7ec87a1ffe1a0fec3f5a721da74ec3a22bc19d5260ad9a9f4516805  super-ttc-iosevka-aile-14.0.1.zip
311aa1167ba8ad288ff236680f0d5c8170d723ef735286d0c7fe5922cd68e8036605ea5edcd99c91b203fe8af41eeea97a9b07bea48c680dbd8426b413631e32  super-ttc-iosevka-etoile-14.0.1.zip
"
