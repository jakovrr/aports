# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=calls
pkgver=41.1
pkgrel=0
pkgdesc="A phone dialer and call handler"
# s390x and riscv64 blocked by polkit -> modemmanager
arch="all !s390x !riscv64"
url="https://gitlab.gnome.org/GNOME/calls"
license="GPL-3.0-or-later"
depends="modemmanager callaudiod"
makedepends="
	callaudiod-dev
	evolution-data-server-dev
	feedbackd-dev
	folks-dev
	gettext-dev
	gobject-introspection-dev
	gom-dev
	gsound-dev
	gstreamer-dev
	gtk+3.0-dev
	libhandy1-dev
	libpeas-dev
	meson
	modemmanager-dev
	ninja
	sofia-sip-dev
	vala
	"
subpackages="$pkgname-lang"
source="https://gitlab.gnome.org/GNOME/calls/-/archive/$pkgver/calls-$pkgver.tar.gz"
options="!check" # Requires running Wayland compositor

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}
sha512sums="
933f85aecd265e2e9d5d34c17ca6f405722cef357c920f6b7dcc599e7ee36dfe6c521d3f7200d88ee4439507c12c71c4d516c2282e4f7da5a173bfdebed2b2ec  calls-41.1.tar.gz
"
