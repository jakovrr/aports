# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwidgetsaddons
pkgver=5.91.0
pkgrel=0
pkgdesc="Addons to QtWidgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only AND Unicode-DFS-2016"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="
	mesa-dri-swrast
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ktwofingertaptest, ktwofingerswipetest and and ksqueezedtextlabelautotest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E '(ktooltipwidget|ktwofingertap|ktwofingerswipe|ksqueezedtextlabelauto)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fcd75424e967de92ac35e96896f9ec75bf06b664e6f2ad2405894c3558718f3b1f89164b9a5ec4ddb9821c00995f7c1cd4209303a0068e03b3521490a909c64c  kwidgetsaddons-5.91.0.tar.xz
"
