# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=modemmanager-qt
pkgver=5.91.0
pkgrel=0
pkgdesc="Qt wrapper for ModemManager DBus API"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> modemmanager
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	modemmanager-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz-dev
	qt5-qttools-dev
	"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # requires dbus running
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
b26a11bfc84136aa6ce7927a6f216926c13e61d358aac3199b5676683c15befc96b0c8102c2ae09946088a885338b9d6e022bf69827c5ffbfeeaa1e2920cfbac  modemmanager-qt-5.91.0.tar.xz
"
