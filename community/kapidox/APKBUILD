# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kapidox
pkgver=5.91.0
pkgrel=0
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Scripts and data for building API documentation (dox) in a standard format and style"
url="https://community.kde.org/Frameworks"
license="BSD-3-Clause"
depends="
	doxygen
	py3-jinja2
	py3-yaml
	python3
	"
makedepends="
	extra-cmake-modules
	py3-setuptools
	python3-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kapidox-$pkgver.tar.xz"
subpackages="$pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
0bf0cb1e3f280ffdb37dea2abe3f100368b0eb24a47a710f2bc2d53c532a6c12443d6499c6c03afe1b1e0696334f68eb34029e69a1ffdeaf80c214c3e839e8ee  kapidox-5.91.0.tar.xz
"
