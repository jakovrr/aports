# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-compiler-rt
# match llvm ver
pkgver=12.0.1
_llvmver="${pkgver%%.*}"
_wasi_sdk_ver=14
pkgrel=0
pkgdesc="WASI LLVM compiler runtime"
url="https://compiler-rt.llvm.org/"
arch="all"
options="!check" # TODO: check
license="Apache-2.0 WITH LLVM-exception BSD-2-Clause"
makedepends="
	clang
	cmake
	libxml2-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	python3-dev
	samurai
	wasi-libc
	zlib-dev
	"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/compiler-rt-$pkgver.src.tar.xz
	https://github.com/WebAssembly/wasi-sdk/archive/refs/tags/wasi-sdk-$_wasi_sdk_ver.tar.gz
	"
builddir="$srcdir"/wasi-compiler-rt

prepare() {
	default_prepare
	mkdir -p "$builddir"
	mv "$srcdir"/compiler-rt-$pkgver.src "$builddir"/compiler-rt
	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/wasi-sdk.cmake "$builddir"
	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/cmake "$builddir"
}

build() {
	export CFLAGS="$CFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCOMPILER_RT_BAREMETAL_BUILD=ON \
		-DCOMPILER_RT_INCLUDE_TESTS=OFF \
		-DCOMPILER_RT_HAS_FPIC_FLAG=OFF \
		-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
		-DCOMPILER_RT_OS_DIR=wasi \
		-DWASI_SDK_PREFIX=/usr \
		-DCMAKE_INSTALL_PREFIX=/usr/lib/clang/$pkgver/ \
		compiler-rt/lib/builtins
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
708780aa47ef289a9700eb76c17cc1f3bab19b8245174e612b5f684c48448030263a72eeef1021b10bdffa6ace2df489adcbba9e7a790ccbcd5fe72056f63f92  compiler-rt-12.0.1.src.tar.xz
4fecb3d9c04b91eb2388a9e51d49fbff6f22b81f9945a07ecdbfe479c96dad1e3b673b8bee24842b0dae5294129a9cb35dcf8e5ecf45437a6d01fb6e0fd13645  wasi-sdk-14.tar.gz
"
