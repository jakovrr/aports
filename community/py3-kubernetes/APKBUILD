# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-kubernetes
_pkgname=kubernetes
pkgver=22.6.0
pkgrel=0
pkgdesc="Official Python client library for kubernetes"
url="https://github.com/kubernetes-client/python"
arch="noarch"
license="Apache-2.0"
depends="py3-certifi py3-dateutil py3-google-auth py3-requests
	py3-requests-oauthlib py3-six py3-urllib3
	py3-websocket-client py3-yaml"
makedepends="py3-setuptools"
checkdepends="pytest py3-mock"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/k/kubernetes/kubernetes-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# deselected tests have missing modules
	pytest --ignore=kubernetes/dynamic/test_client.py --ignore=kubernetes/dynamic/test_discovery.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
532e44c1a41447eb1adc98302005d0109e0b9148fd25e1a7a0ae23d3bcfd3e27eeb4d13c480264ea96dd123eab7d19446f518ebdb881d788d1d4d8175aceddd9  py3-kubernetes-22.6.0.tar.gz
"
