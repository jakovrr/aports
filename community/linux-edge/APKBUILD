# Maintainer: Milan P. Stanić <mps@arvanta.net>

_flavor=edge
pkgname=linux-$_flavor
# NOTE: this kernel is intended for testing
# please resist urge to upgrade it blindly
pkgver=5.16.10
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux latest stable kernel"
url="https://www.kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any
	openssl1.1-compat-dev diffutils findutils xz"
options="!strip !check" # no tests
_config=${config:-config-edge.${CARCH}}

subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz"
case $pkgver in
	*.*.0)	source="$source";;
	*.*.*)	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz" ;;
esac

	source="$source
	config-edge.aarch64
	config-edge.armv7
	config-edge.x86_64
	config-edge.riscv64
	config-edge4virt.aarch64
	config-edge4virt.armv7
	config-edge4virt.x86_64
	config-edge4virt.riscv64
	"

arch="armv7 aarch64 x86_64 riscv64"
license="GPL-2.0"

_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors $_f"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-$_f::$CBUILD_ARCH linux-$_f-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

_carch=$CARCH
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
riscv64) _carch="riscv" ;;
esac

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	case $pkgver in
		*.*.0);;
		*)
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N ;;
	esac

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.$CARCH
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=$pkgver-$pkgrel-$_buildflavor
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		riscv64) _install="install dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/$_abi_release/build \
		"$_outdir"/lib/modules/$_abi_release/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package edge "$pkgdir"
}

# subflavors install in $subpkgdir
edge4virt() {
	_package edge4virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=$pkgver-$pkgrel-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-$_abi_release
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.$CARCH "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/$_abi_release
	ln -sf /usr/src/linux-headers-$_abi_release \
		"$subpkgdir"/lib/modules/$_abi_release/build
}

sha512sums="
7a257dd576bc8493595ec7d6f3c9cb6e22c772a8b2dbe735d2485c4f5c56e26a08695546e7e0f1f1cd04a533f25e829361958d4da0b98bf0ba8094dd57a85aaf  linux-5.16.tar.xz
2e9917689f036b5b28ef9bb8d372148ed9dc0dd4460749d0e27aaa871adeb8e67aaeb557f0d891837be2ccb7f64c346e4f52fc23421917409f46d250fc793f53  patch-5.16.10.xz
df6d41f6fb0605743dd76cfb7b8de9e8467d6765e9c17451e19c99e38e54434281aee8540fb87150bf2ffc8ecb45091ca1a94ccf7dd4a56b090ac6806e7bea20  config-edge.aarch64
6c810389160b1ad0855ca2647c217b1abdc47a3810948db3cd179fae517ad4801fd6ea26c4ec5a138df633855867fa2e97f72906a7f9846b469e5b9cd1e0847d  config-edge.armv7
098795cbf8058d277fe082717af27a44784e508c7b3e244a714de300fac8b2c5e353ab74f2750185fbb652a1c5c85d1a2b74cc4b0a69bf4b63ce3ffc95d05233  config-edge.x86_64
f90ccbed640a81987750e4a3d0357650f0b71e5c35f5bd8bd3e5fac4c15f7831504d265cc8451e7c46dd1d7a4d1349d2a758fd19d0eb8c157fbf19c92d79e939  config-edge.riscv64
2b9ae2f4d4bbcf79149a431375f3f604f5066f7f29bde897be99544709af7a85b3f94f8647d92a415b0dbed1265d78feb26001c3b9820b3b2db0628f4733f7b4  config-edge4virt.aarch64
9faef896a55fb84edb8b9a9ed8c6991fd21327edb8a0549568e12afd3f907fceb0f77d8869212426c2ada4362c3aae9dfdc8db0aa6b1c436a396b449c09fc053  config-edge4virt.armv7
ede7fcfc6329810b1c05acdaa9c8ee0015f8af64dba05e03168973a131780930e919e24991b155f8d1abfb2d8b75e63788c77e7389ec275b553ea144f095ffc3  config-edge4virt.x86_64
b18f868a39fede56ec9199e2dfebd3f298865fb6178e8cc49233e233adb67fe00a6fcde45957164dfe9e160e98a59aaadf862ff00b50655e1cd6c623b4c51236  config-edge4virt.riscv64
"
